import static java.lang.System.out;

public class Ex2 {
	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i <= 100; i++) {
					if (i % 2 == 0) {
						out.println(i);
					}
				}
			}
		};

		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = 1; i <= 99; i++) {
					if (i % 2 != 0) {
						out.printf("--%d\n", i);
					}
				}
			}
		};

		t1.start();
		t2.start();

		t1.join();
		t2.join();
	}
}
