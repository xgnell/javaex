import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import static java.lang.System.out;

public class Ex1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Get student informations
		HashMap<String, String> students = new HashMap<String, String>(10);
		for (int i = 0; i < 10; i++) {
			out.printf("Enter information for student %d:\n", i + 1);
			
			out.printf("Phone: ");
			String phone = input.nextLine();

			out.printf("Name: ");
			String name = input.nextLine();

			students.put(phone, name);
		}

		// Loop
		while (true) {
			out.printf("Your action: ");
			String ans = input.nextLine();
			if (ans.equals("x") || ans.equals("X")) {
				break;
			}

			out.println("List of all students:");
			for (Map.Entry<String, String> student : students.entrySet()) {
				out.printf("Phone: %s\n", student.getKey());
				out.printf("Name: %s\n", student.getValue());
				out.println();
			}
		}

		input.close();
	}
}